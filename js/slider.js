const image1 = document.getElementById('img1')
const image2 = document.getElementById('img2')


const precedente = document.getElementById('precedent')
precedente.addEventListener('click', precedentButton)

const suivante = document.getElementById('suivant')
suivante.addEventListener('click', suivantButton)

function precedentButton(){
    image1.classList.add('cachee')
    image2.classList.remove('cachee')

}
function suivantButton(){
    image2.classList.add('cachee')
    image1.classList.remove('cachee')

}